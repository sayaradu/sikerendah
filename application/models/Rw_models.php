<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rw_models extends CI_Model {

    function find_all(){
        $this->db->select("*")->from('village_rws');
        return $this->db->get()->result();
    }

    function find_by($id){
        $this->db->select("*")->from('villages')
            ->where("id", $id)
            ->where("deleted", 0);
        return $this->db->get()->row();
    }

    function find_rw(){
        $query = "select rws.*, count(rws.id) total_rt from village_rws rws left join village_rts rts on rws.id=rts.rw_id group by rws.id;";
        return $this->db->query($query)->result();
    }

    function insert_rw($param){
        $this->db->insert('village_rws', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function find_rt($id){
        $this->db->select("*")->from('village_rts')
            ->where("rw_id", $id)
            ->where("deleted", 0);
        return $this->db->get()->result();
    }

    function insert_rt($param){
        $this->db->insert('village_rts', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
}