<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class School_models extends CI_Model {

    function find_all(){
        $this->db->select("*")->from('schools');
        return $this->db->get()->result();
    }

    function insert_school($param){
        $this->db->insert('schools', $param);
    }

    function find_school_by($id){
        $this->db->select('*')->from('schools')
            ->where('id', $id)
            ->where('deleted', 0);
        return $this->db->get()->row();
    }

    function find_school_report_by($id){
        $year = date('Y', strtotime('-3 year'));
        $this->db->select('*')->from('school_details')
            ->where('school_id', $id)
            ->where('year >=', $year)
            ->where('deleted', 0);
        return $this->db->get()->result();
    }

    function find_school_detail_by($id){
        $this->db->select('*')->from('school_details')
            ->where('school_id', $id)
            ->where('deleted', 0);
        return $this->db->get()->result();
    }

    function insert_school_detail($param) {
        $this->db->insert('school_details', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function sum_civitas($year, $type = null){
        $query = "";
        if (empty($type)) {
            $query = "select sum(sd.student_male) male, sum(sd.student_female) female from schools s left join school_details sd on s.id=sd.school_id where sd.year=".$year;
        }else{
            $query = "select sum(sd.student_male) male, sum(sd.student_female) female from schools s left join school_details sd on s.id=sd.school_id where sd.year=".$year." and s.type='".$type."'";
        }
        return $this->db->query($query)->row();
    }

    function sum_guru($year, $type = null){
        $query = "";
        if (empty($type)) {
            $query = "select sum(tmp.teacher) total, tmp.type from (select sd.*, s.type from school_details sd left join schools s on s.id= sd.school_id where sd.year=".$year.") tmp group by tmp.type";
        }else{
            $query = "select sum(sd.teacher) teacher from schools s left join school_details sd on s.id=sd.school_id where sd.year=".$year." and s.type='".$type."'";
        }
        return $this->db->query($query)->result();
    }
    
    function school_total(){
        $query = "select count(type) total, type from schools group by type";
        return $this->db->query($query)->result();
    }

    function school_type(){
        $query = "select type from schools group by type";
        return $this->db->query($query)->result();
    }

    function get_year(){
        $query = "select year from school_details group by year";
        return $this->db->query($query)->result();
    }

    function total_school(){
        $query = "select count(*) total from schools where deleted=0";
        return $this->db->query($query)->row();
    }
}