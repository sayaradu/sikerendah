<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Health_models extends CI_Model {

    function find_all(){
        $this->db->select("*")->from('health_facilities');
        return $this->db->get()->result();
    }

    function create_kesehatan($param){
        $this->db->insert('health_facilities', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function health_type_report(){
        $query = "select count(type) total, type from health_facilities group by type";
        return $this->db->query($query)->result();
    }

    function health_employee_report(){
        $query = "select sum(sum_employee) total, type from health_facilities group by type";
        return $this->db->query($query)->result();
    }

    function total_facilities(){
        $query = "select count(*) total from health_facilities where deleted=0";
        return $this->db->query($query)->row();
    }
}