<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    function get_user($username){
        $this->db->select("*")->from('user')->where('username', $username);
        return $this->db->get()->row();
    }
}