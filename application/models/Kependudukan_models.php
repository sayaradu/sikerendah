<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kependudukan_models extends CI_Model {

    function find_all(){
        $query = "select temp.*, fm.name name from (select fc.*, rt.rt_number rt_num, rw.rw_number rw_num from family_cards fc left join village_rts rt on rt.id=fc.rt_id left join village_rws rw on rw.id=rt.rw_id) temp
        left join (select * from family_members where status='kepala') fm on fm.family_card_number=temp.kk_number;";
        return $this->db->query($query)->result();
    }

    function create_house($param){
        $this->db->insert('family_cards', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function find_by($id){
        $this->db->select("*")->from('family_cards')
            ->where('id', $id)
            ->where('deleted', 0);
        return $this->db->get()->row();
    }

    function find_family($number){
        $this->db->select("*")->from('family_members')
            ->where('family_card_number', $number)
            ->where('move', 0)
            ->where('deleted', 0);
        return $this->db->get()->result();
    }

    function create_family($param){
        $this->db->insert('family_members', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function report_education(){
        $query = "select count(education) total, education from family_members group by education";
        return $this->db->query($query)->result();
    }
    
    function report_profession(){
        $query = "select count(profession) total, profession from family_members group by profession";
        return $this->db->query($query)->result();
    }

    function merried_status(){
        $query = "select count(merried) total, merried from family_members group by merried";
        return $this->db->query($query)->result();
    }

    function gender_status(){
        $query = "select count(gender) total, gender from family_members group by gender";
        return $this->db->query($query)->result();
    }

    function find_all_family_mamber(){
        $this->db->select('birth_day')->from('family_members');
        return $this->db->get()->result();
    }

    function total_people(){
        $query = "select count(*) total from family_members where deleted=0";
        return $this->db->query($query)->row();
    }

    function total_kk(){
        $query = "select count(*) total from family_cards where deleted=0";
        return $this->db->query($query)->row();
    }

    function member_move($param){
        $this->db->insert('family_moves', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function change_move($param){
        $this->db->set('move', 1);
        $this->db->where('id', $param['family_member_id']);
        $this->db->update('family_members');
    }

    function get_all_move(){
        $query = "select m.*, fm.name from family_moves m left join family_members fm on fm.id=m.family_member_id where fm.deleted=0";
        return $this->db->query($query)->result();
    }
}