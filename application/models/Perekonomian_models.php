<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Perekonomian_models extends CI_Model {

    function find_all($id = null){
        $query = "";
        if (empty($id)) {
            $query = "select b.*, bt.name economy_name, et.name ecomony_type_name from business b left join business_types bt on bt.id=b.business_type_id 
            left join economy_types et on et.id = b.economy_type_id where b.deleted = 0";
            return $this->db->query($query)->result();
        }else{
            $query = "select b.*, bt.name economy_name, et.name ecomony_type_name from business b left join business_types bt on bt.id=b.business_type_id 
            left join economy_types et on et.id = b.economy_type_id where b.id = ".$id." and  b.deleted = 0";
            return $this->db->query($query)->row();
        }
    
    }

    function find_all_busniss_type(){
        $this->db->select('*')->from('business_types')->where('deleted', 0);
        return $this->db->get()->result();
    }

    function find_all_economy_type(){
        $this->db->select('*')->from('economy_types')->where('deleted', 0);
        return $this->db->get()->result();
    }

    function create_busines($param){
        $this->db->insert('business', $param);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function report_omzet(){
        $query = "select count(omzet) total, omzet from business group by omzet";
        return $this->db->query($query)->result();
    }
    function report_profit(){
        $query = "select count(profit) total, profit from business group by profit";
        return $this->db->query($query)->result();
    }

    function report_bussines(){
        $query = "select count(b.business_type_id) total, bt.name name from business b left join business_types bt on bt.id=b.business_type_id group by b.business_type_id";
        return $this->db->query($query)->result();
    }

    function total_kegiatan(){
        $query = "select count(*) total from business where deleted=0";
        return $this->db->query($query)->row();
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('business');
    }
}