<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->model('School_models', 'school');
        $this->load->model('Health_models', 'health');
        $this->load->model('Kependudukan_models', 'house');
        $this->load->model('Rw_models', 'rw');
        $this->load->model('Perekonomian_models', 'economy');
    }

	public function school()
	{
        $data = $this->school->find_all();
        $this->datatable->set($data);
        $this->datatable->render();
    }

    public function school_detail($id)
	{
        $data = $this->school->find_school_detail_by($id);
        $this->datatable->set($data);
        $this->datatable->render();
    }

    public function generate_school_report()
	{
        $year = $this->input->get('year');
        $type = $this->input->get('type');
        $data = $this->school->sum_civitas($year, $type);
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
    	echo json_encode($data);
    }

    public function kependudukan()
	{
        $data = $this->house->find_all();
        $this->datatable->set($data);
        $this->datatable->render();
    }

    public function family_member($number)
	{
        $data = $this->house->find_family($number);
        $this->datatable->set($data);
        $this->datatable->render();
    }

    public function rw(){
        $data = $this->rw->find_rw();
        $this->datatable->set($data);
        $this->datatable->render();
    }

    public function rt($id){
        $data = $this->rw->find_rt($id);
        $this->datatable->set($data);
        $this->datatable->render();
    }

    public function get_economy($id = null){
        $data = $this->economy->find_all($id);
        if (empty($id)) {
            $this->datatable->set($data);
            $this->datatable->render();
        }else{
            header('Content-type: application/json');
            echo json_encode($data);
        }
        
    }

    public function health(){
        $data = $this->health->find_all();
        $this->datatable->set($data);
        $this->datatable->render();
    }

    public function get_pindah(){
        $data = $this->house->get_all_move();
        $this->datatable->set($data);
        $this->datatable->render();
    }
}
