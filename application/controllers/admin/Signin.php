<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('login') == true) {
            redirect('admin/dashboard');
        }
        $this->load->model('User_model', 'user');
    }

	public function index()
	{
        if($this->input->post()){
            $password = md5($this->input->post('password'));
            $admin = $this->user->get_user($this->input->post('username'));
            if(!empty($admin)){
                if ($admin->password == $password) {
                    unset($admin->password);
                    $this->session->set_userdata('login', true);
                    $this->session->set_userdata('user', $admin);
                    redirect(base_url('/admin/dashboard'));
                }else{
                    $this->session->set_flashdata('error', 'Kombinasi username dan password tidak sesuai.');
                    $this->load->view('admin/signin');
                }
            }else{
                $this->session->set_flashdata('error', 'Username tidak dikenal.');
                $this->load->view('admin/signin');
            }
        }else{
            $this->load->view('admin/signin');
        }
		
	}
}
