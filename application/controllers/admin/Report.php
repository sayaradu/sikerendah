<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        if ($this->session->userdata('login') == false) {
            redirect(base_url('/admin/signin'));
		}
		$this->load->model('School_models', 'school');
    }

	public function sekolah($id)
	{
		$data['school_id'] = $id;
		$data['report'] = $this->school->find_school_report_by($id);
		$data['school'] = $this->school->find_school_by($id);
		// var_dump($data['school']);
		$data['user'] = $this->session->userdata('user');
		$this->load->view('admin/navigation', $data);
		$this->load->view('admin/report/sekolah');
	}
	
	public function add_data_sekolah($id){
		$data = [
			'school_id' => $id,
			'teacher' => $this->input->post('jumlah_guru'),
			'year' => $this->input->post('data_year'),
			'student_male' => $this->input->post('jumlah_siswa_laki'),
			'student_female' => $this->input->post('jumlah_siswa_perempuan'),
		];
		if ($this->school->insert_school_detail($data)) {
			$this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
			redirect("admin/report/sekolah/".$id);
		}
	}
}
