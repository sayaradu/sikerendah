<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kesehatan extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->model('Health_models', 'health');
		$this->load->model('Rw_models', 'rw');
    }

	public function index()
	{
		$data['user'] = $this->session->userdata('user');
		$data['rws'] = $this->rw->find_all();
		$data['kesehatan'] = $this->health->find_all();
		$data['health_type_report'] = $this->health->health_type_report();
		$data['health_employee_report'] = $this->health->health_employee_report();
		$this->load->view('user/navigation', $data);
		$this->load->view('user/kesehatan');
	}

	public function add_kesehatan(){
		$data = [
            'name' => $this->input->post('name'),
            'type' => $this->input->post('type'),
            'rw' => $this->input->post('rw'),
            'rt' => $this->input->post('rt'),
            'address' => $this->input->post('address'),
            'sum_employee' => $this->input->post('sum_employee'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
        ];
        // var_dump($data);die;
        if ($this->health->create_kesehatan($data)) {
            $this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
            redirect("user/kesehatan");
        }
	}
}
