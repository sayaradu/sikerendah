<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->model('School_models', 'school');
        $this->load->model('Health_models', 'health');
        $this->load->model('Kependudukan_models', 'house');
        $this->load->model('Rw_models', 'rw');
        $this->load->model('Perekonomian_models', 'economy');
    }

	public function index()
	{
		$data['user'] = $this->session->userdata('user');
		$data['total_fasilitas_kesehatan'] = $this->health->total_facilities();
		$data['total_people'] = $this->house->total_people();
		$data['jumlah_kk'] = $this->house->total_kk();
		$data['fasilitas_pendidikan'] = $this->school->total_school();
		$data['kegitan_ekonomi'] = $this->economy->total_kegiatan();
		$this->load->view('user/navigation', $data);
		$this->load->view('user/dashboard');
	}
}
