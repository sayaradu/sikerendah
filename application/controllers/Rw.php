<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rw extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->model('Rw_models', 'rw');
    }

	public function index()
	{
        $data['user'] = $this->session->userdata('user');
        $data["rw_list"] = $this->rw->find_rw();
        $this->load->view('user/navigation', $data);
		$this->load->view('user/rw_list');
    }
    
    public function add_rw(){
        if ($this->input->post()) {
            $data = [
                "rw_number" => $this->input->post("rw_number"),
                "chairman_name" => $this->input->post("chairman_name"),
                "phone" => $this->input->post("phone"),
                "address" => $this->input->post("address"),
                "lat" => $this->input->post("lat"),
                "lng" => $this->input->post("lng"),
            ];
            if ($this->rw->insert_rw($data)) {
                $this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
                redirect("user/rw");
            }
        }
    }
    
    public function detail_rw($id){
        $data['user'] = $this->session->userdata('user');
        $data["list_rt"] = $this->rw->find_rt($id);
        $data["rw_id"] = $id;
        $this->load->view('user/navigation', $data);
		$this->load->view('user/rt_list');
    }

    public function add_rt(){
        if ($this->input->post()) {
            $data = [
                "rw_id" => $this->input->post("rw_id"),
                "rt_number" => $this->input->post("rt_number"),
                "total_kk" => $this->input->post("total_kk"),
                "chairman_name" => $this->input->post("chairman_name"),
                "phone" => $this->input->post("phone"),
                "address" => $this->input->post("address"),
                "lat" => $this->input->post("lat"),
                "lng" => $this->input->post("lng"),
            ];
            if ($this->rw->insert_rt($data)) {
                $this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
                redirect("user/rw/detail_rw/".$this->input->post("rw_id"));
            }
        }
    }
}
