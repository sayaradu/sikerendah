<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Move extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kependudukan_models', 'house');
        $this->load->model('Rw_models', 'rw');
    }

    public function index(){
        $data['user'] = $this->session->userdata('user');
        $this->load->view('user/navigation', $data);
        $this->load->view('user/pindah'); 
    }
}
