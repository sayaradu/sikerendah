<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perumahan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kependudukan_models', 'house');
        $this->load->model('Rw_models', 'rw');
    }

    public function index()
    {

        $birth = $this->house->find_all_family_mamber();
        $balita = 0;
        $anakanak = 0;
        $remaja_awal = 0;
        $remaja_akhir = 0;
        $dewasa_awal = 0;
        $dewasa_akhir = 0;
        $lansia_awal = 0;
        $lansia_akhir = 0;
        $manula = 0;
        foreach ($birth as $value) {
            $birthDate = $value->birth_day;
            $birthDate = explode("/", $birthDate);
            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                ? ((date("Y") - $birthDate[2]) - 1)
                : (date("Y") - $birthDate[2]));
            if ($age <= 5) {
                $balita += 1;
            }else if($age > 5 && $age <= 11){
                $anakanak += 1;
            }else if($age > 11 && $age <= 16){
                $remaja_awal += 1;
            }else if($age > 16 && $age <= 25){
                $remaja_akhir += 1;
            }else if($age > 25 && $age <= 35){
                $dewasa_awal += 1;
            }else if($age > 35 && $age <= 45){
                $dewasa_akhir += 1;
            }else if($age > 45 && $age <= 55){
                $lansia_awal += 1;
            }else if($age > 55 && $age <= 65){
                $lansia_akhir += 1;
            }else{
                $manula += 1;
            }
        }
        $data['report_usia'] = [
            'balita' => $balita,
            'anak_anak' => $anakanak,
            'remaja_awal' => $remaja_awal,
            'remaja_akhir' => $remaja_akhir,
            'dewasa_awal' =>$dewasa_awal,
            'dewasa_akhir' => $dewasa_akhir,
            'lansia_awal' => $lansia_awal,
            'ansia_akhir' => $lansia_akhir,
            'manula' => $manula,
        ];
        // var_dump($data['report_usia']);die;
        $data['report_education'] = $this->house->report_education();
        $data['report_profession'] = $this->house->report_profession();
        $data['rts'] = $this->rw->find_rt(6);
        $data['merried'] = $this->house->merried_status();
        $data['gender'] = $this->house->gender_status();
        $data['user'] = $this->session->userdata('user');
        $this->load->view('user/navigation', $data);
        $this->load->view('user/perumahan');
    }

    public function map($id)
    {
        $data['id'] = $id;
        $data['user'] = $this->session->userdata('user');
        $data['perumahan'] = $this->house->find_by($id);
        $this->load->view('user/navigation', $data);
        $this->load->view('user/perumahan_map');
    }

    public function add_kependudukan()
    {
        $data = [
            'kk_number' => $this->input->post('kk_number'),
            'address' => $this->input->post('address'),
            'rt_id' => $this->input->post('rt'),
            'rw_id' => 6,
            'block' => $this->input->post('block'),
            'num_rum' => $this->input->post('num_rum'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
        ];
        if ($this->house->create_house($data)) {
            $this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
            redirect("user/perumahan");
        }
    }

    public function add_family()
    {
        $id  = $this->input->post('id');
        $data = [
            'family_card_number' => $this->input->post('family_card_number'),
            'name' => $this->input->post('name'),
            'birth_day' => $this->input->post('birth_day'),
            'birth_place' => $this->input->post('birth_place'),
            'gender' => $this->input->post('gender'),
            'phone' => $this->input->post('phone') ? $this->input->post('phone') : null,
            'status' => $this->input->post('status'),
            'education' => $this->input->post('education'),
            'profession' => $this->input->post('profession'),
            'merried' => $this->input->post('merried'),
        ];
        if ($this->house->create_family($data)) {
            $this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
            redirect("user/perumahan/map/" . $id);
        }
    }

    public function upload_data()
    {
        if (isset($_POST['import'])) {
            $file = $_FILES['file_upload']['tmp_name'];
            // var_dump($file);die;
            // Medapatkan ekstensi file csv yang akan diimport.
            $ekstensi  = explode('.', $_FILES['file_upload']['name']);

            // Tampilkan peringatan jika submit tanpa memilih menambahkan file.
            if (empty($file)) {
                echo 'File tidak boleh kosong!';
            } else {
                // Validasi apakah file yang diupload benar-benar file csv.
                if (strtolower(end($ekstensi)) === 'csv' && $_FILES["file_upload"]["size"] > 0) {

                    $i = 0;
                    $handle = fopen($file, "r");
                    $result = [];
                    while (($row = fgetcsv($handle, 2048))) {
                        $i++;
                        if ($i == 1) continue;

                        // Data yang akan disimpan ke dalam databse
                        $data = [
                            'family_card_number' => $row[0],
                            'name' =>  $row[1],
                            'birth_day' =>  $row[2],
                            'birth_place' =>  $row[3],
                            'gender' =>  $row[4],
                            'status' =>  $row[5],
                            'education' =>  $row[6],
                            'profession' =>  $row[7],
                            'merried' =>  $row[8],
                            'phone' =>  $row[9],
                        ];

                        // Simpan data ke database.
                        if ($this->house->create_family($data) == false) {
                            $data['created'] = false;
                            array_push($result, $data);
                        } else {
                            $data['created'] = true;
                            array_push($result, $data);
                        }
                    }
                    // var_dump($failed);die;
                    fclose($handle);
                    $this->session->set_flashdata('data', $result);
                    redirect("user/perumahan/upload_result");
                } else {
                    $this->session->set_flashdata('message', 'Upload file gagal, format tidak didukung.');
                    redirect("user/perumahan");
                }
            }
        }
    }

    public function upload_result()
    {
        $data['user'] = $this->session->userdata('user');
        $this->load->view('user/navigation', $data);
        $this->load->view('user/upload_result');
    }
}
