<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perekonomian extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kependudukan_models', 'house');
        $this->load->model('Rw_models', 'rw');
        $this->load->model('Perekonomian_models', 'economy');
    }

    public function index()
    {
        $data['business_type'] = $this->economy->find_all_busniss_type();
        $data['economy_type'] = $this->economy->find_all_economy_type();
        $data['economy_list'] =  $this->economy->find_all();
        $data['report_omzet'] =  $this->economy->report_omzet();
        $data['report_profit'] =  $this->economy->report_profit();
        $data['report_bussiness'] = $this->economy->report_bussines();
        $data['user'] = $this->session->userdata('user');
        $this->load->view('user/navigation', $data);
        $this->load->view('user/perekonomian');
    }

    public function add_perekonomian(){
        $data = [
            'business_name' => $this->input->post('business_name'),
            'owner' => $this->input->post('owner'),
            'address' => $this->input->post('address'),
            'pos' => $this->input->post('pos'),
            'area' => $this->input->post('area'),
            'economy_type_id' => $this->input->post('economy_type_id'),
            'business_type_id' => $this->input->post('business_type_id'),
            'initial_capital' => $this->input->post('initial_capital'),
            'omzet' => $this->input->post('omzet'),
            'profit' => $this->input->post('profit'),
            'salary' => $this->input->post('salary'),
            'employee' => $this->input->post('employee'),
            'delivery' => $this->input->post('delivery'),
            'working_day' => $this->input->post('working_day'),
            'open' => $this->input->post('open_hours'),
            'close' => $this->input->post('close_hours'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
        ];
        // var_dump($data);
        if ($this->economy->create_busines($data)) {
            $this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
            redirect("user/perekonomian");
        }
    }
}
