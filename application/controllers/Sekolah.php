<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->model('School_models', 'school');
    }

	public function index()
	{
		$data['year'] = 2019;
		$data['school_total'] = $this->school->school_total();
		$data['school_type'] = $this->school->school_type();
		$data['year_list'] = $this->school->get_year();
		$data['civitas_guru'] = $this->school->sum_guru($data['year']);
		$data['civitas_sekolah'] = $this->school->sum_civitas($data['year']);
		$data['sekolah'] = $this->school->find_all();
		$data['user'] = $this->session->userdata('user');
		$this->load->view('user/navigation', $data);
		$this->load->view('user/sekolah');
	}

	public function add(){
		if ($this->input->post()) {
			$data = [
				"nss" => $this->input->post("nss"),
				"school_name" => $this->input->post("name"),
				"address" => $this->input->post("address"),
				"type" => $this->input->post("school_type"),
				"lat" => $this->input->post("lat"),
				"lng" => $this->input->post("lng"),
				"phone" => $this->input->post("phone"),
			];
			$this->db->insert('schools', $data);
			$this->session->set_flashdata('message', 'Data berhasil ditambahkan.');
			redirect("user/sekolah");
		}else{
			$data['user'] = $this->session->userdata('user');
			$this->load->view('user/navigation', $data);
			$this->load->view('user/tambah_sekolah');
		}
	}
}
