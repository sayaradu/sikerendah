<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Rukun Tetangga</h2>
                    <div class="page-breadcrumb">
                        <!-- <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/kelurahan') ?>" class="breadcrumb-link">Kelurahan</a></li>
                            </ol>
                        </nav> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-header">
                Rukun Tetangga 
                <a href="#" style="float: right"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-rt">Add</button></a>
            </div>
            <div class="card-body">
                <?php if ($this->session->flashdata("message")) { ?>
                    <div class="alert alert-primary" role="alert">
                        <?= $this->session->flashdata("message") ?>
                    </div>
                <?php } ?>
                <table id="rukun_tetangga" class="display">
                    <thead>
                        <tr>
                            <th>RT</th>
                            <th>Ketua RT</th>
                            <th>Telepon</th>
                            <th>Alamat</th>
                            <th>Total KK</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="modal fade" id="add-rt" tabindex="-1" role="dialog" aria-labelledby="add-rt" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-rt">Tambah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="<?= base_url('/admin/rw/add_rt') ?>">
                        <div class="modal-body">
                            <input type="text" class="form-control" value="<?= $rw_id ?>" id="rw_id" name="rw_id" hidden>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nomor RT</label>
                                <input type="text" class="form-control" id="rt_number" name="rt_number" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ketua RT</label>
                                <input type="text" class="form-control" id="chairman_name" name="chairman_name" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Total KK</label>
                                <input type="text" class="form-control" id="total_kk" name="total_kk" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone</label>
                                <input type="text" class="form-control" id="phone" name="phone" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Address</label>
                                <input type="text" class="form-control" id="address" name="address" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lat</label>
                                <input type="text" class="form-control" id="lat" name="lat" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lng</label>
                                <input type="text" class="form-control" id="lng" name="lng" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" value="save" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="card-header">
                    Location
                </div>
                <div class="card-body">
                    <div id="map" class="gmaps"></div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>


<script>
     $(document).ready(function() {
        $('#rukun_tetangga').DataTable({
            ajax: "<?= base_url('admin/api/rt/'.$rw_id) ?>",
            columns: [
                {
                    data: "rt_number"
                },
                {
                    data: "chairman_name"
                },
                {
                    data: "phone"
                },
                {
                    data: "address"
                },
                {
                    data: "total_kk"
                }
            ]
        });
    });
</script>


<script>
    function initMap() {
        var locations = [
            <?php foreach($list_rt as $vill) {?> 
                ['RT <?= $vill->rt_number ?>', <?= $vill->lat ?>, <?= $vill->lng ?>],
            <?php } ?>
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(-6.977897, 107.632706),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCInqxaHuA2rNlzx4kEFLkKuaU8Wfbmots&callback=initMap" async defer></script>



</body>

</html>