<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Upload Result</h2>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="card-body">
                    <table class="display table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Status Keluraga</th>
                                <th>Status Menikah</th>
                                <th>Pendidikan</th>
                                <th>Pekerjaan</th>
                                <th>Telepon</th>
                                <th>Hasil</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($this->session->flashdata("data")) { ?>
                                <div class="alert alert-primary" role="alert">
                                    <?php foreach ($this->session->flashdata("data") as $data) { ?>
                                        <tr style="background-color:<?= $data["created"] ? "#a6e06c" : "#eb7575" ?>">
                                            <td><?= $data["family_card_number"] ?></td>
                                            <td><?= $data["birth_day"] ?></td>
                                            <td><?= $data["birth_place"] ?></td>
                                            <td><?= $data["gender"] ?></td>
                                            <td><?= $data["status"] ?></td>
                                            <td><?= $data["education"] ?></td>
                                            <td><?= $data["profession"] ?></td>
                                            <td><?= $data["merried"] ?></td>
                                            <td><?= $data["phone"] ?></td>
                                            <td><?= $data["created"] ? "sukses" : "gagal" ?></td>
                                        </tr>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end wrapper -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>


<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUb3jDWJQ28vDJhuQZxkC0NXr_zycm8D0&callback=initMap" async defer></script>


</body>

</html>