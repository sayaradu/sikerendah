<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Dashboard </h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Dashbaord</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Dashboard Report
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-4 col-lg-3 col-md-3 col-sm-12">
                        <div class="card">
                            <h5 class="card-header">Jumlah Masyarakat</h5>
                            <div class="card-body">
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?= $total_people->total  ?></h1>
                                </div>
                            </div>
                            <div class="card-footer text-center bg-white">
                                <a href="../admin/perumahan" class="card-link">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3 col-md-3 col-sm-12">
                        <div class="card">
                            <h5 class="card-header">Fasilitas Kesehatan</h5>
                            <div class="card-body">
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?= $total_fasilitas_kesehatan->total  ?></h1>
                                </div>
                            </div>
                            <div class="card-footer text-center bg-white">
                                <a href="../admin/kesehatan" class="card-link">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3 col-md-3 col-sm-12">
                        <div class="card">
                            <h5 class="card-header">Jumlah KK Mekarsari Endah</h5>
                            <div class="card-body">
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?= $jumlah_kk->total  ?></h1>
                                </div>
                            </div>
                            <div class="card-footer text-center bg-white">
                                <a href="../admin/perumahan" class="card-link">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3 col-md-3 col-sm-12">
                        <div class="card">
                            <h5 class="card-header">Fasilitas Pendidikan</h5>
                            <div class="card-body">
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?= $fasilitas_pendidikan->total  ?></h1>
                                </div>
                            </div>
                            <div class="card-footer text-center bg-white">
                                <a href="../admin/sekolah" class="card-link">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3 col-md-3 col-sm-12">
                        <div class="card">
                            <h5 class="card-header">Kegiatan Ekonomi</h5>
                            <div class="card-body">
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?= $kegitan_ekonomi->total  ?></h1>
                                </div>
                            </div>
                            <div class="card-footer text-center bg-white">
                                <a href="../admin/perekonomian" class="card-link">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>

</body>

</html>
