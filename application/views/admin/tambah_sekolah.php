<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Sekolah</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/sekolah') ?>" class="breadcrumb-link">Sekolah</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Body here -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-header">
                <!-- <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-school">Tambah Sekolah</button> -->
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <form method="post" action="<?= base_url("/admin/sekolah/add") ?>">
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Nama Sekolah</label>
                                <input id="name" name="name" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">NSS</label>
                                <input id="nss" name="nss" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Type</label>
                                <select name="school_type" class="form-control" id="village" required>
                                    <option>Pilih Tipe</option>
                                    <option value="TK">TK</option>
                                    <option value="MA">MA</option>
                                    <option value="MTs">MTs</option>
                                    <option value="PAUD">PAUD</option>
                                    <option value="SD">SD</option>
                                    <option value="SMP">SMP</option>
                                    <option value="SMA">SMA</option>
                                    <option value="SMK">SMK</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Alamat</label>
                                <input id="address" name="address" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Phone</label>
                                <input id="phone" name="phone" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Latitude</label>
                                <input id="lat" name="lat" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Longnitude</label>
                                <input id="lng" name="lng" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input id="submit" type="submit" value="Sumbit" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>

<script>

</script>
</body>

</html>