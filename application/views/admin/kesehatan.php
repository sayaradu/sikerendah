<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Kesehatan</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/kesehatan') ?>" class="breadcrumb-link">Kesehatan</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Body here -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-header">
                <a href="#" style="float: right; margin-left:4px;"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-kesehatan">Tambah Data</button></a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="sekolah" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Tipe</th>
                            <th>Alamat</th>
                            <th>RW</th>
                            <th>RT</th>
                            <th>Jumlah Pegawai</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
              </div>
            </div>
        </div>



        <div class="modal fade" id="add-kesehatan" tabindex="-1" role="dialog" aria-labelledby="add-kesehatan" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-kesehatan">Tambah Data Kesehatan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="<?= base_url('/admin/kesehatan/add_kesehatan') ?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nama Fasilitas</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>

                            <div class="form-group">
                                <label>Tipe Fasilitas</label>
                                <select name="type" class="form-control" id="type" required>
                                    <option>Pilih Tipe</option>
                                    <option value="puskesmas">puskesmas</option>
                                    <option value="apotik">apotik</option>
                                    <option value="posyandu">posyandu</option>
                                    <option value="rumah sakit">rumah sakit</option>
                                    <option value="bidan">bidan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>RW</label>
                                <select name="rw" class="form-control" id="rw" onchange="get_rt()" required>
                                    <option>Pilih RW</option>
                                    <?php foreach ($rws as $rw) { ?>
                                        <option value="<?= $rw->id ?>"><?= $rw->rw_number ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>RT</label>
                                <select name="rt" class="form-control" id="rt" required>
                                    <option>Pilih RT</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Jumlah Pegawai</label>
                                <input type="number" class="form-control" id="sum_employee" name="sum_employee" required>
                            </div>
                            <div class="form-group">
                                <label>Alamat Lengkap</label>
                                <input type="text" class="form-control" id="address" name="address" required>
                            </div>
                            <div class="form-group">
                                <label>Lat</label>
                                <input type="text" class="form-control" id="lat" name="lat" required>
                            </div>
                            <div class="form-group">
                                <label>Lng</label>
                                <input type="text" class="form-control" id="lng" name="lng" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" value="save" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Location
            </div>
            <div class="card-body">
                <div id="map" class="gmaps"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Report
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="type_kesehatan" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Civitas</h5> -->
                            <div class="card-body">
                                <div id="health_employee_report" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->

<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>

<script>
    $(document).ready(function() {
        $('#sekolah').DataTable({
            ajax: "<?= base_url('admin/api/health') ?>",
            columns: [{
                    data: "name"
                },
                {
                    data: "type"
                },
                {
                    data: "address"
                },
                {
                    data: "rw"
                },
                {
                    data: "rt"
                },
                {
                    data: "sum_employee"
                }
            ]
        });
    });
</script>

<script>
    function get_rt() {
        var rw = document.getElementById('rw').value
        var select = document.getElementById("rt");
        // select.options[select.selectedIndex] = null;
        $('#rt')
            .find('option')
            .remove()
            .end()
            .append('<option>Pilih RT</option>')
            .val('Pilih RT');
        axios.get('<?= base_url('admin/api/rt/') ?>' + rw).then(result => {
            result.data.data.map((val, i) => {
                var select = document.getElementById("rt");
                var option = document.createElement("option");
                option.text = val.rt_number;
                option.value = val.id;
                select.appendChild(option);
            })
        })
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Type', 'Jumlah'],
            <?php foreach ($health_type_report as $data) { ?>['<?= $data->type ?>', <?= $data->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Jumlah Fasilitas Per Kategori'
        };

        var education = new google.visualization.PieChart(document.getElementById('type_kesehatan'));

        education.draw(data, options);
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Type', 'Jumlah'],
            <?php foreach ($health_employee_report as $data) { ?>['<?= $data->type ?>', <?= $data->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Jumlah Karyawan Per Kategori'
        };

        var education = new google.visualization.PieChart(document.getElementById('health_employee_report'));

        education.draw(data, options);
    }
</script>

<script>
    function initMap() {
        var locations = [
            <?php foreach ($kesehatan as $data) { ?>['<?= $data->name ?>', <?= $data->lat ?>, <?= $data->lng ?>],
            <?php } ?>
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(-6.977897, 107.632706),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCInqxaHuA2rNlzx4kEFLkKuaU8Wfbmots&callback=initMap" async defer></script>

</body>

</html>
