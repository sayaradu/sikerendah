<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title"><?= $perumahan->kk_number ?></h2>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="card-body">
                    <?= $perumahan->address ?>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <a href="#" style="float: right; margin-left:4px;"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-perumahan">Tambah Data</button></a>
                <!-- <a href="#" style="float: right"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#upload-perumahan">Upload</button></a> -->
            </div>
            <div class="card-body">
                <?php if ($this->session->flashdata("message")) { ?>
                    <div class="alert alert-primary" role="alert">
                        <?= $this->session->flashdata("message") ?>
                    </div>
                <?php } ?>
                <div class="table-responsive">
                <table id="family" class="display">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Jenis Kelamin</th>
                            <th>Status Keluraga</th>
                            <th>Status Menikah</th>
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                            <th>Telepon</th>
                            <th>Ubah Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
              </div>
            </div>
        </div>

        <div class="modal fade" id="add-perumahan" tabindex="-1" role="dialog" aria-labelledby="add-perumahan" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-perumahan">Add New Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="<?= base_url('/admin/perumahan/add_family') ?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" value="<?= $perumahan->kk_number ?>" class="form-control" id="family_card_number" name="family_card_number" hidden>
                                <input type="text" value="<?= $id ?>" class="form-control" id="id" name="id" hidden>
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control" id="birth_place" name="birth_place" required>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <!-- <input type="text" class="form-control" id="birth_day" name="birth_day" required> -->
                                <input class="form-control" id="birth_day" name="birth_day" />
                                <script>
                                    $('#birth_day').datepicker();
                                </script>
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <select name="gender" class="form-control" id="gender" required>
                                    <option>Pilih Jenis Kelamin</option>
                                    <option value="L">Laki Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Status Keluarga</label>
                                <select name="status" class="form-control" id="status" required>
                                    <option>Pilih Status keluarga</option>
                                    <option value="kepala">Kepala</option>
                                    <option value="istri">Istri</option>
                                    <option value="anak">Anak</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pekerjaan</label>
                                <input type="text" class="form-control" id="profession" name="profession" required>
                            </div>
                            <div class="form-group">
                                <label>Pendidikan</label>
                                <select name="education" class="form-control" id="education" required>
                                    <option>Pilih Pendidikan</option>
                                    <option value="SD/Sederajat">SD/Sederajat</option>
                                    <option value="SLTP/Sederajat">SLTP/Sederajat</option>
                                    <option value="SLTA/Sederajat">SLTA/Sederajat</option>
                                    <option value="D3">D3</option>
                                    <option value="D4">D4</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Status Menikah</label>
                                <select name="merried" class="form-control" id="merried" required>
                                    <option>Pilih Status Menikah</option>
                                    <option value="1">Sudah Menikah</option>
                                    <option value="0">Belum Menikah</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="text" class="form-control" id="phone" name="phone" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" value="save" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pindah" tabindex="-1" role="dialog" aria-labelledby="pindah" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="pindah">Pindah Alamat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="<?= base_url('/admin/perumahan/pindah') ?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" value="" class="form-control" id="family_id" name="family_id" hidden>
                                <input type="text" value="<?= $id ?>" class="form-control" id="id" name="id" hidden>
                            </div>
                            <div class="form-group">
                                <label>Alamat Tujuan</label>
                                <input type="text" class="form-control" id="destination" name="destination" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" value="save" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-body">
                <div class="card-header">
                    Location
                </div>
                <div class="card-body">
                    <div id="map" class="gmaps"></div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>


<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>


<script>
    $(document).ready(function() {
        $('#family').DataTable({
            ajax: "<?= base_url('admin/api/family_member/' . $perumahan->kk_number) ?>",
            columns: [{
                    data: "name"
                },
                {
                    data: "birth_day"
                },
                {
                    data: "birth_place"
                },
                {
                    data: "gender"
                },
                {
                    data: "status"
                },
                {
                    render: function(data, type, row) {
                        if (row.merried == 1) {
                            return "Sudah Menikah"
                        }
                        return "Belum Menikah"
                    }
                },
                {
                    data: "education"
                },
                {
                    data: "profession"
                },
                {
                    data: "phone"
                },
                {
                    render: function(data, type, row) {
                        return '<a onclick="openmodal('+row.id+')" data-toggle="modal" data-target="#pindah"><button type="button" class="btn btn-primary btn-sm" id="report">Pindah</button></a>';
                    }
                }
            ]
        });
    });
</script>

<script>
    function openmodal(id){
        console.log(id)
        document.getElementById("family_id").value = id
    }
</script>

<script>
    function initMap() {

        var locations = [['Rumah',
            <?= $perumahan->lat ?>,
            <?= $perumahan->lng ?>,
        ]];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(-6.977897, 107.632706),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCInqxaHuA2rNlzx4kEFLkKuaU8Wfbmots&callback=initMap" async defer></script>


</body>

</html>
