<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Report Sekolah </h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Dashbaord</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

        <div class="card">
            <h5 class="card-header">Markers</h5>
            <div class="card-body">
                <div id="map_2" class="gmaps"></div>
            </div>
        </div>

        <div class="card">
            <!-- <div class="card-header">
            </div> -->
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Perbandingan Total Sekolah</h5>
                            <div class="card-body">
                                <div id="sekolah" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Civitas</h5>
                            <div class="card-body">
                                <div id="civitas" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script src="<?= base_url("assets/vendor/charts/c3charts/c3.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/charts/c3charts/d3-5.4.0.min.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/gmaps.min.js") ?>"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyBUb3jDWJQ28vDJhuQZxkC0NXr_zycm8D0&amp;sensor=true"></script>
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApP2r35HK5WjnRydZcSaJ1CCzigZWqjAk&callback=initMap"
  type="text/javascript"></script> -->

<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>


<script>
    var map_2;
    map_2 = new GMaps({
        div: '#map_2',
        lat: -6.960118,
        lng: 107.636200
    });
    <?php foreach ($list_sekolah as $sekolah) { ?>
        map_2.addMarker({
            lat: "<?= $sekolah->lat ?>",
            lng: "<?= $sekolah->lng ?>",
            title: 'Lima',
            details: {
                database_id: 42,
                author: 'HPNeo'
            },
            // click: function(e) {
            //     if (console.log)
            //         console.log(e);
            //     alert('You clicked in this marker');
            // }
        });
    <?php  } ?>

    // map_2.addMarker({
    //     lat: -12.042,
    //     lng: -77.028333,
    //     title: 'Marker with InfoWindow',
    //     infoWindow: {
    //         content: '<p>HTML Content</p>'
    //     }
    // });

    var chart = c3.generate({
        bindto: "#sekolah",
        color: {
            pattern: ["#5969ff", "#ff407b", "#25d5f2", "#ffc750", "#59d0ff", "#ffac59", "#d1c5b8", "#4a7680"]
        },
        data: {
            columns: [
                <?php foreach ($perbandingan_sekolah as $data) { ?>["<?= $data->school_type ?> ( <?= $data->total ?> )", <?= $data->total ?>],
                <?php } ?>
            ],
            type: 'pie',

        }
    });

    var civitas = c3.generate({
        bindto: "#civitas",
        color: {
            pattern: ["#5969ff", "#ff407b", "#25d5f2"]
        },
        data: {
            columns: [
                ["Guru ( <?= $report_civitas->guru ?> )", <?= $report_civitas->guru  ?>],
                ["Siswa ( <?= $report_civitas->siswa ?> )", <?= $report_civitas->siswa  ?>],
                ["Siswi ( <?= $report_civitas->siswi ?> )", <?= $report_civitas->siswi  ?>]
            ],
            type: 'pie',

        }
    });

    setTimeout(function() {
        chart.load({

        });
    }, 1500);

    setTimeout(function() {
            chart.unload({
                ids: 'data1'
            });
            chart.unload({
                ids: 'data2'
            });
        },
        2500
    );
</script>


</body>

</html>
