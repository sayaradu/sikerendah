<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>">
    <link href="<?=base_url('assets/vendor/fonts/circular-std/style.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url('assets/libs/css/style.css') ?>">
    <link rel="stylesheet" href="<?=base_url('assets/vendor/fonts/fontawesome/css/fontawesome-all.css') ?>">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center"><a href=""><h1 style="color:#0d47a1">SIKERENDAH</h1></a><span class="splash-description">Please login with your account.</span></div>
            <div class="card-body">
                <?php if (!empty($this->session->flashdata('error'))) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $this->session->flashdata('error') ?>
                    </div>
                <?php } ?>
                <form method="post" action="">
                    <div class="form-group">
                        <input class="form-control form-control-lg" name="username" id="username" type="text" placeholder="Username" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" name ="password" id="password" type="password" placeholder="Password" required>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                </form>
            </div>
        </div>
    </div>
  
    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="<?=base_url('assets/vendor/jquery/jquery-3.3.1.min.js') ?>"></script>
    <script src="<?=base_url('assets/vendor/bootstrap/js/bootstrap.bundle.js') ?>"></script>
</body>
 
</html>