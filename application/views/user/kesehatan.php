<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Kesehatan</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/kesehatan') ?>" class="breadcrumb-link">Kesehatan</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Body here -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-header">
                Fasilitas Kesehatan
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="sekolah" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Tipe</th>
                                <th>Alamat</th>
                                <th>RW</th>
                                <th>RT</th>
                                <th>Jumlah Pegawai</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Location
            </div>
            <div class="card-body">
                <div id="map" class="gmaps"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Report
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="type_kesehatan" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Civitas</h5> -->
                            <div class="card-body">
                                <div id="health_employee_report" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->

<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>

<script>
    $(document).ready(function() {
        $('#sekolah').DataTable({
            ajax: "<?= base_url('api/health') ?>",
            columns: [{
                    data: "name"
                },
                {
                    data: "type"
                },
                {
                    data: "address"
                },
                {
                    data: "rw"
                },
                {
                    data: "rt"
                },
                {
                    data: "sum_employee"
                }
            ]
        });
    });
</script>

<script>
    function get_rt() {
        var rw = document.getElementById('rw').value
        var select = document.getElementById("rt");
        // select.options[select.selectedIndex] = null;
        $('#rt')
            .find('option')
            .remove()
            .end()
            .append('<option>Pilih RT</option>')
            .val('Pilih RT');
        axios.get('<?= base_url('api/rt/') ?>' + rw).then(result => {
            result.data.data.map((val, i) => {
                var select = document.getElementById("rt");
                var option = document.createElement("option");
                option.text = val.rt_number;
                option.value = val.id;
                select.appendChild(option);
            })
        })
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Type', 'Jumlah'],
            <?php foreach ($health_type_report as $data) { ?>['<?= $data->type ?>', <?= $data->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Jumlah Fasilitas Per Kategori'
        };

        var education = new google.visualization.PieChart(document.getElementById('type_kesehatan'));

        education.draw(data, options);
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Type', 'Jumlah'],
            <?php foreach ($health_employee_report as $data) { ?>['<?= $data->type ?>', <?= $data->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Jumlah Karyawan Per Kategori'
        };

        var education = new google.visualization.PieChart(document.getElementById('health_employee_report'));

        education.draw(data, options);
    }
</script>

<script>
    function initMap() {
        var locations = [
            <?php foreach ($kesehatan as $data) { ?>['<?= $data->name ?>', <?= $data->lat ?>, <?= $data->lng ?>],
            <?php } ?>
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(-6.977897, 107.632706),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCInqxaHuA2rNlzx4kEFLkKuaU8Wfbmots&callback=initMap" async defer></script>

</body>

</html>