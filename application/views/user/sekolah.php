<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Sekolah</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/sekolah') ?>" class="breadcrumb-link">Sekolah</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Body here -->
        <!-- ============================================================== -->

        <div class="card">
            <div class="card-header">
                Sekolah
            </div>
            <div class="card-body">
                <?php if ($this->session->flashdata("message")) { ?>
                    <div class="alert alert-primary" role="alert">
                        <?= $this->session->flashdata("message") ?>
                    </div>
                <?php } ?>
                <div class="table-responsive">
                    <table id="sekolah" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Nama Sekolah</th>
                                <th>Alamat</th>
                                <th>NSS</th>
                                <th>Report</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Location
            </div>
            <div class="card-body">
                <div id="map" class="gmaps"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Report
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="sekolah_report" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Civitas</h5> -->
                            <div class="card-body">
                                <div id="perbandingan_guru" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Civitas</h5> -->
                            <div class="card-body">
                                <div id="civitas" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <!-- <label for="inputText3" class="col-form-label">Type Sekolah</label> -->
                                <select name="school_type" class="form-control" id="school_type" required>
                                    <option>Pilih Type Sekolah</option>
                                    <?php foreach ($school_type as $type) { ?>
                                        <option value="<?= $type->type ?>"><?= $type->type ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <!-- <label for="inputText3" class="col-form-label">Tahun</label> -->
                                <select name="year" class="form-control" id="year" required>
                                    <option>Pilih Tahun</option>
                                    <?php foreach ($year_list as $year) { ?>
                                        <option value="<?= $year->year ?>"><?= $year->year ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <!-- <label for="inputText3" class="col-form-label">Submit</label> -->
                                <button onclick="generateReport()" class="btn btn-primary">Generate Report</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div id="filter_report_sekolah" style="height: 200;"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>

<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    $(document).ready(function() {
        $('#sekolah').DataTable({
            ajax: "<?= base_url('api/school') ?>",
            responsive: true,
            columns: [{
                    data: "type"
                },
                {
                    data: "school_name"
                },
                {
                    data: "address"
                },
                {
                    data: "nss"
                },
                {
                    render: function(data, type, row) {
                        return '<a href="<?= base_url('report/sekolah/') ?>' + row.id + '"><button type="button" class="btn btn-primary btn-sm" id="report">Detail</button></a>';
                    }
                }
            ]
        });
    });
</script>

<script>
    function initMap() {
        var locations = [
            <?php foreach ($sekolah as $data) { ?>['<?= $data->school_name ?>', <?= $data->lat ?>, <?= $data->lng ?>],
            <?php } ?>
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(-6.977897, 107.632706),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChartGlobal);

    function drawChartGlobal() {

        var data = google.visualization.arrayToDataTable([
            ['Civitas', 'Jumlah'],
            ['Siswi', <?= $civitas_sekolah->female  ?>],
            ['Siswa', <?= $civitas_sekolah->male  ?>]
        ]);

        var data_sekolah = google.visualization.arrayToDataTable([
            ['Sekolah', 'Jumlah'],
            <?php foreach ($school_total as $school) { ?>['<?= $school->type ?>', <?= $school->total ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Banyak civitas sekolah di Kelurahan Baleendah'
        };

        var civitas = new google.visualization.PieChart(document.getElementById('civitas'));
        var chart_sekolah = new google.visualization.PieChart(document.getElementById('sekolah_report'));

        civitas.draw(data, options);
        chart_sekolah.draw(data_sekolah, {
            title: "Banyak sekolah di kelurahan Baleendah"
        });
    }

    function drawChartReport() {

        var data = google.visualization.arrayToDataTable([
            ['Civitas', 'Jumlah'],
            // ['Guru', teacher],
            ['Siswi', female],
            ['Siswa', male]
        ]);

        var options = {
            title: 'Banyak civitas sekolah di Kelurahan Baleendah'
        };

        var civitas = new google.visualization.PieChart(document.getElementById('filter_report_sekolah'));

        civitas.draw(data, options);
    }


    function generateReport() {
        var year = document.getElementById('year').value
        var school_type = document.getElementById('school_type').value
        axios.get('<?= base_url('api/generate_school_report') ?>?year=' + year + '&type=' + school_type).then(result => {
            if (result.data.male == null) {
                document.getElementById('filter_report_sekolah').innerHTML = "<div id='embeded'>Data tidak ditemukan.</div>"
            } else {
                document.getElementById('filter_report_sekolah').innerHTML = "<div id='embeded'></div>"
                google.charts.load('current', {
                    'packages': ['corechart']
                });
                google.charts.setOnLoadCallback(() => {
                    console.log(result.data)
                    var data = google.visualization.arrayToDataTable([
                        ['Civitas', 'Jumlah'],
                        ['Siswi', parseInt(result.data.female)],
                        ['Siswa', parseInt(result.data.male)]
                    ]);

                    var options = {
                        title: 'Perbandingan jumlah civitas ' + school_type + ' se Baleendah tahun ' + year
                    };

                    var civitas = new google.visualization.PieChart(document.getElementById('filter_report_sekolah'));

                    civitas.draw(data, options);
                });
            }

        })

    }
</script>


<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Guru', 'Jumlah'],
            <?php foreach ($civitas_guru as $guru) { ?>['<?= $guru->type ?>', <?= $guru->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Banyak Guru di Kelurahan Baleendah'
        };

        var education = new google.visualization.PieChart(document.getElementById('perbandingan_guru'));

        education.draw(data, options);
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCInqxaHuA2rNlzx4kEFLkKuaU8Wfbmots&callback=initMap" async defer></script>

</body>

</html>
