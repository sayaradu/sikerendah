<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Perekonomian</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/perumahan') ?>" class="breadcrumb-link">Perekonomian</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-header">
                Perekonomian
            </div>
            <div class="card-body">
                <?php if ($this->session->flashdata("message")) { ?>
                    <div class="alert alert-primary" role="alert">
                        <?= $this->session->flashdata("message") ?>
                    </div>
                <?php } ?>
                <div class="table-responsive">
                    <table id="perumahan" class="display">
                        <thead>
                            <tr>
                                <th>Nama Ekonomi</th>
                                <th>Pemilik</th>
                                <th>Alamat</th>
                                <th>Modal Awal</th>
                                <th>Omzet</th>
                                <th>Keuntungan</th>
                                <th>Hari Kerja</th>
                                <th>Jam Kerja</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-perumahan" tabindex="-1" role="dialog" aria-labelledby="add-perumahan" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-perumahan">Tambah Data Perekonomian</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="<?= base_url('/admin/perekonomian/add_perekonomian') ?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nama Kegiatan Ekonomi</label>
                                <input type="text" class="form-control" id="business_name" name="business_name" required>
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" class="form-control" id="owner" name="owner" required>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" class="form-control" id="address" name="address" required>
                            </div>
                            <div class="form-group">
                                <label>Kode Pos</label>
                                <input type="text" class="form-control" id="pos" name="pos" required>
                            </div>
                            <div class="form-group">
                                <label>Luas Area</label>
                                <input type="text" class="form-control" id="area" name="area" required>
                            </div>
                            <div class="form-group">
                                <label>Tipe Ekonomi</label>
                                <select name="economy_type_id" class="form-control" onchange="" id="economy_type_id" required>
                                    <option>Pilih Tipe Ekonomi</option>
                                    <?php foreach ($economy_type as $economy) { ?>
                                        <option value="<?= $economy->id ?>"><?= $economy->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tipe Bisnis</label>
                                <select name="business_type_id" class="form-control" onchange="" id="business_type_id" required>
                                    <option>Pilih Tipe Bisnis</option>
                                    <?php foreach ($business_type as $business) { ?>
                                        <option value="<?= $business->id ?>"><?= $business->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Modal Awal</label>
                                <!-- <input type="text" class="form-control" id="initial_capital" name="initial_capital" required> -->
                                <select name="initial_capital" class="form-control" onchange="" id="initial_capital" required>
                                    <option>Pilih Modal Awal</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Omset</label>
                                <!-- <input type="text" class="form-control" id="omzet" name="omzet" required> -->
                                <select name="omzet" class="form-control" onchange="" id="omzet" required>
                                    <option>Pilih Omzet</option>
                                    <option>Pilih Keuntungan</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Keuntungan</label>
                                <!-- <input type="text" class="form-control" id="profit" name="profit" required> -->
                                <select name="profit" class="form-control" onchange="" id="profit" required>
                                    <option>Pilih Keuntungan</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Jumlah Karyawan</label>
                                <input type="text" class="form-control" id="employee" name="employee" required>
                            </div>
                            <div class="form-group">
                                <label>Gaji Karyawan</label>
                                <!-- <input type="text" class="form-control" id="salary" name="salary" required> -->
                                <select name="salary" class="form-control" onchange="" id="salary" required>
                                    <option>Pilih Gaji Karyawan</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Menyediakan Kirim Barang</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" value="1" id="customControlValidation2" name="delivery" required>
                                    <label class="custom-control-label" for="customControlValidation2">Ya</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" class="custom-control-input" value="0" id="customControlValidation3" name="delivery" required>
                                    <label class="custom-control-label" for="customControlValidation3">Tidak</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Hari Kerja</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" value="senin - jumat" id="working_day1" name="working_day" required>
                                    <label class="custom-control-label" for="working_day1">Senin - Jumat</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" class="custom-control-input" value="senin - minggu" id="working_day2" name="working_day" required>
                                    <label class="custom-control-label" for="working_day2">Setiap Hari</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Jam Buka</label>
                                <input type="text" class="form-control" id="open_hours" name="open_hours" required>
                            </div>
                            <div class="form-group">
                                <label>Jam Tutup</label>
                                <input type="text" class="form-control" id="close_hours" name="close_hours" required>
                            </div>
                            <div class="form-group">
                                <label>Lat</label>
                                <input type="text" class="form-control" id="lat" name="lat" required>
                            </div>
                            <div class="form-group">
                                <label>Lng</label>
                                <input type="text" class="form-control" id="lng" name="lng" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" value="save" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-body">
                <div class="card-header">
                    Location
                </div>
                <div class="card-body">
                    <div id="map" class="gmaps"></div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Report
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="omset" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Civitas</h5> -->
                            <div class="card-body">
                                <div id="profit_report" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="report_bussiness" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="detail">Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nama Kegiatan Ekonomi</label>
                                <input type="text" class="form-control" value="" id="edit_business_name" name="business_name" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" class="form-control" value="" id="edit_owner" name="owner" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" class="form-control" value="" id="edit_address" name="address" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Kode Pos</label>
                                <input type="text" class="form-control" value="" id="edit_pos" name="pos" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Luas Area</label>
                                <input type="text" class="form-control" id="edit_area" name="area" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Tipe Ekonomi</label>
                                <select name="economy_type_id" class="form-control" onchange="" id="edit_economy_type_id" required disabled>
                                    <option>Pilih Tipe Ekonomi</option>
                                    <?php foreach ($economy_type as $economy) { ?>
                                        <option value="<?= $economy->id ?>"><?= $economy->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tipe Bisnis</label>
                                <select name="business_type_id" class="form-control" onchange="" id="edit_business_type_id" required disabled>
                                    <option>Pilih Tipe Bisnis</option>
                                    <?php foreach ($business_type as $business) { ?>
                                        <option value="<?= $business->id ?>"><?= $business->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Modal Awal</label>
                                <!-- <input type="text" class="form-control" id="initial_capital" name="initial_capital" required> -->
                                <select name="initial_capital" class="form-control" onchange="" id="edit_initial_capital" required disabled>
                                    <option>Pilih Modal Awal</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Omset</label>
                                <!-- <input type="text" class="form-control" id="omzet" name="omzet" required> -->
                                <select name="omzet" class="form-control" onchange="" id="edit_omzet" required disabled>
                                    <option>Pilih Omzet</option>
                                    <option>Pilih Keuntungan</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Keuntungan</label>
                                <!-- <input type="text" class="form-control" id="profit" name="profit" required> -->
                                <select name="profit" class="form-control" onchange="" id="edit_profit" required disabled>
                                    <option>Pilih Keuntungan</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Jumlah Karyawan</label>
                                <input type="text" class="form-control" id="edit_employee" name="employee" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Gaji Karyawan</label>
                                <!-- <input type="text" class="form-control" id="salary" name="salary" required> -->
                                <select name="salary" class="form-control" onchange="" id="edit_salary" required disabled>
                                    <option>Pilih Gaji Karyawan</option>
                                    <option value="< 100000">
                                        < Rp 100.000,00 </option> <option value="100000 - 500000">Rp 100.000,00 - Rp 500.000,00
                                    </option>
                                    <option value="500000 - 1000000">Rp 500.000,00 - Rp 1.000.000,00</option>
                                    <option value="1000000 - 10000000">Rp 1.000.000,00 - Rp 10.000.000,00</option>
                                    <option value="10000000 - 50000000">Rp 10.000.000,00 - Rp 50.000.000,00</option>
                                    <option value="50000000 - 100000000">Rp 50.000.000,00 - Rp 100.000.000,00</option>
                                    <option value="> 100000000"> > Rp 100.000.000,00</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Menyediakan Kirim Barang</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" value="1" id="edit_customControlValidation2" name="delivery" required disabled>
                                    <label class="custom-control-label" for="edit_customControlValidation2">Ya</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" class="custom-control-input" value="0" id="edit_customControlValidation3" name="delivery" required disabled>
                                    <label class="custom-control-label" for="edit_customControlValidation3">Tidak</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Hari Kerja</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" value="senin - jumat" id="edit_working_day1" name="working_day" required disabled>
                                    <label class="custom-control-label" for="edit_working_day1">Senin - Jumat</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" class="custom-control-input" value="senin - minggu" id="edit_working_day2" name="working_day" required disabled>
                                    <label class="custom-control-label" for="edit_working_day2">Setiap Hari</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Jam Buka</label>
                                <input type="text" class="form-control" id="edit_open_hours" name="open_hours" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Jam Tutup</label>
                                <input type="text" class="form-control" id="edit_close_hours" name="close_hours" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Lat</label>
                                <input type="text" class="form-control" id="edit_lat" name="lat" required disabled>
                            </div>
                            <div class="form-group">
                                <label>Lng</label>
                                <input type="text" class="form-control" id="edit_lng" name="lng" required disabled>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>

</div>

</div>

<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Rata Rata Omset', 'Jumlah'],
            <?php foreach ($report_omzet as $omzet) { ?>['<?= $omzet->omzet ?>', <?= $omzet->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Omzet perusahaan'
        };

        var education = new google.visualization.PieChart(document.getElementById('omset'));

        education.draw(data, options);
    }
</script>


<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Tipe Bisnis', 'Jumlah'],
            <?php foreach ($report_bussiness as $bussines) { ?>['<?= $bussines->name ?>', <?= $bussines->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Tipe bisnis perusahaan'
        };

        var education = new google.visualization.PieChart(document.getElementById('report_bussiness'));

        education.draw(data, options);
    }
</script>


<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Rata Rata Profit', 'Jumlah'],
            <?php foreach ($report_profit as $profit) { ?>['<?= $profit->profit ?>', <?= $profit->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Profit Perusahaan'
        };

        var education = new google.visualization.PieChart(document.getElementById('profit_report'));

        education.draw(data, options);
    }
</script>

<script>
    $(document).ready(function() {
        $('#perumahan').DataTable({
            ajax: "<?= base_url('api/get_economy') ?>",
            columns: [{
                    data: "business_name"
                },
                {
                    data: "owner"
                },
                {
                    data: "address"
                },
                {
                    data: "initial_capital"
                },
                {
                    data: "omzet"
                },
                {
                    data: "profit"
                },

                {
                    data: "working_day"
                },
                {
                    render: function(data, type, row) {
                        return row.open + '-' + row.close;
                    }
                },
                {
                    render: function(data, type, row) {
                        return '<button type="button" data-toggle="modal" data-target="#detail" onCLick="detail(' + row.id + ')" class="btn btn-primary btn-sm" id="report">Detail</button>';
                    }
                }
            ]
        });
    });
</script>

<script>
    function detail(id) {
        axios.get('<?= base_url('api/get_economy/') ?>' + id).then((result) => {
            console.log(result)
            document.getElementById("edit_business_name").value = result.data.business_name
            document.getElementById("edit_owner").value = result.data.owner
            document.getElementById("edit_address").value = result.data.address
            document.getElementById("edit_pos").value = result.data.pos
            document.getElementById("edit_area").value = result.data.area
            document.getElementById("edit_economy_type_id").value = result.data.economy_type_id
            document.getElementById("edit_business_type_id").value = result.data.business_type_id
            document.getElementById("edit_initial_capital").value = result.data.initial_capital
            document.getElementById("edit_omzet").value = result.data.omzet
            if (result.data.delivery == "1" || result.data.delivery == true) {
                document.getElementById("edit_customControlValidation2").checked = true
            } else {
                document.getElementById("edit_customControlValidation2").checked = true
            }
            if (result.data.working_day == "senin - minggu") {
                document.getElementById("edit_working_day2").checked = true
            } else {
                document.getElementById("edit_working_day1").checked = true
            }
            document.getElementById("edit_employee").value = result.data.employee
            document.getElementById("edit_profit").value = result.data.profit
            document.getElementById("edit_salary").value = result.data.salary
            document.getElementById("edit_open_hours").value = result.data.open
            document.getElementById("edit_close_hours").value = result.data.close
            document.getElementById("edit_lat").value = result.data.lat
            document.getElementById("edit_lng").value = result.data.lng
        }).catch((err) => {

        });
    }
</script>

<script>
    function initMap() {
        var locations = [
            <?php foreach ($economy_list as $econom) { ?>['<?= $econom->business_name ?>', <?= $econom->lat ?>, <?= $econom->lng ?>],
            <?php } ?>
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(-6.977897, 107.632706),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCInqxaHuA2rNlzx4kEFLkKuaU8Wfbmots&callback=initMap" async defer></script>



</body>

</html>