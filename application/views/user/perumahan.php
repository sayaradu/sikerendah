<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Perumahan Mekarsari Endah </h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/perumahan') ?>" class="breadcrumb-link">Perumahan Mekarsari Endah</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <div class="card">
            <div class="card-header">
                Penduduk
            </div>
            <div class="card-body">
                <?php if ($this->session->flashdata("message")) { ?>
                    <div class="alert alert-primary" role="alert">
                        <?= $this->session->flashdata("message") ?>
                    </div>
                <?php } ?>
                <div class="table-responsive">
                    <table id="perumahan" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>RT</th>
                                <th>RW</th>
                                <th>Blok</th>
                                <th>Nomor Rumah</th>
                                <th>Alamat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->

        <div class="card">
            <div class="card-header">
                Report
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="tingkat_pendidikan" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Civitas</h5> -->
                            <div class="card-body">
                                <div id="pekerjaan" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="jenis_kelamin" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Civitas</h5> -->
                            <div class="card-body">
                                <div id="status_kawin" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Perbandingan Total Sekolah</h5> -->
                            <div class="card-body">
                                <div id="report_usia" style="height: 200;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>


<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>

<script>
    function get_rw() {
        var village = document.getElementById('village').value
        console.log(village)
        var select = document.getElementById("rw");
        select.options[select.selectedIndex] = null;
        axios.get('<?= base_url('api/kelurahan_rw/') ?>' + village).then(result => {
            var select = document.getElementById("rw");
            select.options[select.selectedIndex] = null;
            result.data.data.map((val, i) => {
                var select = document.getElementById("rw");
                var option = document.createElement("option");
                option.text = val.rw_number;
                option.value = val.id;
                select.appendChild(option);
            })
        })
    }

    function get_rt() {
        var rw = document.getElementById('rw').value
        console.log(village)
        var select = document.getElementById("rt");
        select.options[select.selectedIndex] = null;
        axios.get('<?= base_url('api/kelurahan_rt/') ?>' + rw).then(result => {
            result.data.data.map((val, i) => {
                var select = document.getElementById("rt");
                var option = document.createElement("option");
                option.text = val.rt_number;
                option.value = val.id;
                select.appendChild(option);
            })
        })
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Pendidikan', 'Jumlah'],
            <?php foreach ($report_education as $edu) { ?>['<?= $edu->education ?>', <?= $edu->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Tingkat Pendidikan'
        };

        var education = new google.visualization.PieChart(document.getElementById('tingkat_pendidikan'));

        education.draw(data, options);
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Jenis Kelamin', 'Jumlah'],
            <?php foreach ($gender as $kelamin) { ?>['<?= $kelamin->gender ?>', <?= $kelamin->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Jenis Kelamin'
        };

        var education = new google.visualization.PieChart(document.getElementById('jenis_kelamin'));

        education.draw(data, options);
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Nikah', 'Jumlah'],
            <?php foreach ($merried as $nikah) { ?>['<?= $nikah->merried ? 'Nikah' : 'Belum Nikah' ?>', <?= $nikah->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Status Pernikahan'
        };

        var education = new google.visualization.PieChart(document.getElementById('status_kawin'));

        education.draw(data, options);
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Pekerjaan', 'Jumlah'],
            <?php foreach ($report_profession as $profession) { ?>['<?= $profession->profession ?>', <?= $profession->total  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Pekerjaan'
        };

        var education = new google.visualization.PieChart(document.getElementById('pekerjaan'));

        education.draw(data, options);
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Usia', 'Jumlah'],
            <?php foreach ($report_usia as  $key => $usia) { ?>['<?= $key ?>', <?= $usia  ?>],
            <?php } ?>
        ]);

        var options = {
            title: 'Perbandingan Usia'
        };

        var education = new google.visualization.PieChart(document.getElementById('report_usia'));

        education.draw(data, options);
    }
</script>


<script>
    $(document).ready(function() {
        $('#perumahan').DataTable({
            ajax: "<?= base_url('api/kependudukan') ?>",
            columns: [{
                    render: function(data, type, row) {
                        if (row.name != null) {
                            return row.name;
                        }else{
                            return 'Data Keluarga Belum Lengkap';
                        }
                    }
                },
                {
                    data: "rt_num"
                },
                {
                    data: "rw_num"
                },
                {
                    data: "block"
                },
                {
                    data: "num_rum"
                },
                {
                    data: "address"
                },
                {
                    render: function(data, type, row) {
                        return '<a href="<?= base_url('/perumahan/map/') ?>' + row.id + '"><button type="button" class="btn btn-primary btn-sm" id="report">Member</button></a>';
                    }
                }
            ]
        });
    });
</script>


</body>

</html>