<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title"><?= $school->school_name ?></h2>
                    <!-- <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Sikerendah</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url('/admin/dashboard') ?>" class="breadcrumb-link">Dashbaord</a></li>
                            </ol>
                        </nav>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

        <!-- <div class="card">
            <h5 class="card-header">Markers</h5>
            <div class="card-body">
                <div id="map_2" class="gmaps"></div>
            </div>
        </div> -->

        <div class="card">
            <div class="card-header">
                Detail
            </div>
            <div class="card-body">
                <?php if ($this->session->flashdata("message")) { ?>
                    <div class="alert alert-primary" role="alert">
                        <?= $this->session->flashdata("message") ?>
                    </div>
                <?php } ?>
                <div class="table-responsive">
                <table id="school_detail" class="display">
                    <thead>
                        <tr>
                            <th>Year</th>
                            <th>Teacher</th>
                            <th>Student Male</th>
                            <th>Student Female</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Report
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Jumlah Guru</h5> -->
                            <div class="card-body">
                                <div id="guru" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <!-- <h5 class="card-header">Jumlah Siswa</h5> -->
                            <div class="card-body">
                                <div id="siswa" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Location
            </div>
            <div class="card-body">
                <div id="map" class="gmaps"></div>
            </div>
        </div>
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="<?= base_url("assets/vendor/jquery/jquery-3.3.1.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.js") ?>"></script>
<script src="<?= base_url("assets/vendor/slimscroll/jquery.slimscroll.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/main-js.js") ?>"></script>
<script src="<?= base_url("assets/libs/js/gmaps.min.js") ?>"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
    });

    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });
</script>


<script>
    $(document).ready(function() {
        $('#school_detail').DataTable({
            ajax: "<?= base_url('api/school_detail/' . $school_id) ?>",
            columns: [{
                    data: "year"
                },
                {
                    data: "teacher"
                },
                {
                    data: "student_male"
                },
                {
                    data: "student_female"
                },
                // {
                //     render: function(data, type, row) {
                //         return '<a href="<?= base_url('admin/report/sekolah/') ?>' + row.id + '"><button type="button" class="btn btn-primary btn-sm" id="report">Report</button></a>';
                //     }
                // }
            ]
        });
    });
</script>

<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['bar']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'male', 'female'],
            <?php foreach ($report as $data) { ?>['<?= $data->year ?>', <?= $data->student_male ?>, <?= $data->student_female ?>],
            <?php } ?>
        ]);

        var options = {
            chart: {
                title: 'Data Siswa <?= $school->school_name ?>',
                subtitle: 'Perbandingan data siswa <?= $school->school_name ?> per tahun',
            }
        };

        var chart = new google.charts.Bar(document.getElementById('siswa'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>

<script>
    google.charts.load('current', {
        'packages': ['bar']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'guru'],
            <?php foreach ($report as $data) { ?>['<?= $data->year ?>', <?= $data->teacher ?>],
            <?php } ?>
        ]);

        var options = {
            chart: {
                title: 'Data Guru <?= $school->school_name ?>',
                subtitle: 'Perbandingan data guru <?= $school->school_name ?> per tahun',
            }
        };

        var chart = new google.charts.Bar(document.getElementById('guru'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>

<script>
    function initMap() {

        var locations = [['<?= $school->school_name ?>',
            <?= $school->lat ?>,
            <?= $school->lng ?>,
        ]];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(-6.977897, 107.632706),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCInqxaHuA2rNlzx4kEFLkKuaU8Wfbmots&callback=initMap" async defer></script>

</body>

</html>
